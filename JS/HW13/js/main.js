let btnGreen = document.getElementById('green-button');
let btnWhite = document.getElementById('white-button');
let body = document.getElementsByTagName('body');
let menu = document.querySelectorAll('.style-menu-links')
window.onload = function () {
    if(localStorage.getItem('bgcolor')!== null) {
        let bodyColor = localStorage.getItem('bgcolor');
        body[0].style.background = bodyColor;
    } if (localStorage.getItem('linkcolor')!== null) {
        let menuColor = localStorage.getItem('linkcolor');
        menu.forEach((link) => { link.style.color = menuColor})
    }
}
btnGreen.addEventListener('click',()=>{
    body[0].style.background = '#16A085';
    menu.forEach((link) => { link.style.color ="blue"})
   localStorage.setItem('bgcolor','#16A085')
   localStorage.setItem('linkcolor','blue')
})
btnWhite.addEventListener('click',()=> {
    body[0].style.background = null;
    menu.forEach((link) => { link.style.color = null})
   localStorage.setItem('bgcolor','null')
   localStorage.setItem('linkcolor','null')
});



