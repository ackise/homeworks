let tab = document.querySelectorAll('.tabs li')
let text = document.querySelectorAll('.tabs-content li')

for (let i = 0; i<tab.length; i++) {
    tab[i].classList.add('tabs-title')
    tab[i].addEventListener('click',function () {
        for ( let j=0; j<tab.length; j++) {
            tab[j].classList.remove('active')
            text[j].style.display = 'none'
        }
        text[i].style.display = 'block'
        this.classList.add('active')
    })
}