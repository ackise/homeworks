function getListFromArray(arr) {
    let list = document.createElement('ul')
    arr.map((element) => {
        list.innerHTML += `<li>${element}</li>`
    })
    document.body.prepend(list)
};

getListFromArray(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
