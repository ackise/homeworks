let input = document.getElementById('input');
let error = document.getElementById('error');
let spanPrice = document.createElement('span');

document.body.prepend(spanPrice);

input.onblur = function () {
    if (input.value <=0) {
        input.classList.add('invalid');
        input.classList.remove('number-input-onfocus')
        error.innerHTML = " Please put correct price"
    } else {
        input.classList.add('number-input-onfocus')
        input.classList.remove('invalid')
        error.innerHTML = ""
        spanPrice.innerHTML = 'Current price:' + input.value + '<i class="icon-remove-circle"></i>'
        spanPrice.classList.add('currentPrice')

let close = document.querySelector('.icon-remove-circle')
close.addEventListener('click', ()=> {
    spanPrice.classList.remove('currentPrice')
    spanPrice.innerHTML = ""
    input.value = ""
});
    }
};
input.onfocus = function () {
    input.classList.add('number-input-onfocus')
};



