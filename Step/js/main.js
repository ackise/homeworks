
// Our Service tabs
$('ul.tabsTitles').on('click', 'li:not(.active)', function() {
    $(this)
        .addClass("active")
        .siblings()
        .removeClass("active")
        .closest("div.tabs")
        .find("div.tabsContent")
        .removeClass("active")
        .eq($(this).index())
        .addClass("active");
});

// our amazing filter 
let page = 1;
$(document).ready(function(){
    $('.menuItemsWork').click(function(){
        let filter = $(this).attr('id') ;     
        let show = $('.picSeT');
      $('.menuItemsWork').removeClass("menuItemsWork-active");
      $(this).toggleClass("menuItemsWork-active");
   
      if(filter == "all"){ 
          let elem = 12 * page;
          $(show).removeClass('Visible').addClass('noVisible');
          $('.itemsList li').slice(0,elem).removeClass('noVisible').addClass('Visible')
      }
      else{
          $(show).not("."+filter).removeClass('Visible').addClass('noVisible');
          $(show).filter("."+filter).addClass('Visible').removeClass('noVisible');
      }
    });
  })

// Load more
$('.itemsList li').addClass('Visible');
$('.itemsList li').slice(12).addClass('noVisible').removeClass('Visible')
$('.loadMore').click(() => {
    page++;
  $('.preLoad').css('display','block')
  $('.loadMore').hide()
  setTimeout(getImg,2000);
  function getImg(){
    $('.itemsList li').slice(12).removeClass('noVisible').addClass('Visible')
    $('.preLoad').css('display','none')
  }
});
    
    

// slider photos
$('.slider1').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    asNavFor: '.slider2'
  });
  $('.slider2').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider1',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    arrows: true,
    nextArrow: '<i class="rightarrow fas fa-chevron-right"></i>',
    prevArrow: '<i class="leftarrow fas fa-chevron-left"></i>',
  });

  // gallery mansory

  $('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: 380,
    horizontalOrder: true,
    gutter: 10
  });
  
  $('.iall').masonry({
    itemSelector: '.item',
    gutter: 10
  });

  $('.iall2').masonry({
    itemSelector: '.items',
    gutter: 3,
    horizontalOrder: true,
    columnWidth: 124,

  });